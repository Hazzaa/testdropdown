//
//  ViewController.swift
//  testMenu
//
//  Created by mac on 5/2/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import RSSelectionMenu

class ViewController: UIViewController {

    @IBOutlet weak var myButton: UIButton!
    
    
    @IBAction func ayhaga(_ sender: Any) {
        showAsPopover(myButton)
    }
    
    let dataArray = ["Sachin Tendulkar", "Rahul Dravid", "Saurav Ganguli", "Virat Kohli", "Suresh Raina", "Ravindra Jadeja", "Chris Gyle", "Steve Smith", "Anil Kumble", "Ahmed Hazzaa", "moaaz"]
    
    var selectedDataArray = [String]()
    var myArray = [String]()
    
    var firstRowSelected = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func showAsPopover(_ sender: UIView) {
        
        // Show as Popover with datasource
        
        let selectionMenu = RSSelectionMenu(selectionType: .Multiple, dataSource: dataArray, cellType: .Basic) { (cell, object, indexPath) in
            cell.textLabel?.text = object
        }
        
        
        selectionMenu.setSelectedItems(items: selectedDataArray) { (text, isSelected, selectedItems) in
            
            // update your existing array with updated selected items, so when menu presents second time updated items will be default selected.
            self.selectedDataArray = selectedItems
            self.myArray = selectedItems
        }
        
        // show as popover
        // Here specify popover sourceView and size of popover
        // specifying nil will present with default size
        selectionMenu.show(style: .Popover(sourceView: sender, size: nil), from: self)
    }

    
    
    
    
    
}

